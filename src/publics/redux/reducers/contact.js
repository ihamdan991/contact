const initialValue = {
  data: [
    {
      id : 1,
      name: 'Hamdan',
      address: 'Cirebon'
    },
    {
      id : 2,
      name: 'Diki',
      address: 'Tuban'
    },
    {
      id : 3,
      name: 'Arif',
      address: 'Tegal'
    },
    {
      id : 4,
      name: 'Tama',
      address: 'Tangerang'
    },
    {
      id : 5,
      name: 'Rifki',
      address: 'Pekalongan'
    }
  ],
  isLoading: false,
  isError: false,
  isFinish: false
}

export default (state = initialValue, action) => {
  switch (action.type) {
    case 'GET_CONTACT_PENDING':
      return {
        ...state,
        isLoading: true
      }

    case 'GET_CONTACT_FULFILLED':
      return {
        ...state,
        isLoading: false,
        isFinish: true,
        data: action.payload.data
      };

    case 'GET_CONTACT_REJECTED':
      return {
        ...state,
        isLoading: false,
        isError: true,
        data: 'Error Network'
      };

    case 'CREATE_CONTACT': 
      return {
        ...state,
        data: [
          ...state.data,
          action.payload
        ]
      }
    
      case 'DELETE_CONTACT': 
      return {
        ...state,
        data: state.data.filter(contact => contact.id !== action.payload)
      }

    default:
      return state;
  }
}
