import React from 'react';
import { connect } from 'react-redux';
import { StyleSheet, View, FlatList, Alert, TouchableOpacity } from 'react-native';
import axios from 'axios';
import { Text, Body, Fab, SwipeRow, Button, Icon, Left, CheckBox } from 'native-base'

import { get, deleteContact } from '../../publics/redux/actions/contact';

class Contact extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      active: false,
      toggle: false
    }
  }

  renderItem = ({ item, index }) => {
    return (
      <SwipeRow
        key={index}
        rightOpenValue={-75}
        body={
          <TouchableOpacity
            onPress={() =>
              this.props.navigation.navigate("ContactEdit", {
                id: item.id,
                name: item.name,
                address: item.address
              })
            }>
            <Body>
              <View style={{ flex: 1, flexDirection: 'row' }}>
                {this.state.toggle  ? 
                  <View style={{ paddingTop: 10 }}>
                    <CheckBox checked={false} />
                  </View> : <View/>
                }
                
                <View style={{ marginLeft: 10 }}>
                  <Text>{item.name}</Text>
                  <Text>{item.address}</Text>
                </View>
              </View>
            </Body>
          </TouchableOpacity>

        }
        right={
          <Button danger onPress={() => this.handleDelete(item.id, item.name)}>
            <Icon active name="trash" />
          </Button>
        }
      />
    )
  }

  handleCreate = () => {
    this.props.dispatch({
      type: 'Navigation/NAVIGATE',
      routeName: 'ContactCreate'
    })
  }


  handleDelete = (id, name) => {
    Alert.alert(
      "Delete " + name,
      "Are you sure want to delete contact ?",
      [
        {
          text: "NO", onPress: () => {
            console.log("Cancel delete");
          }
        },
        {
          text: "YES", onPress: () => {
            console.log("Confirm delete");
            this.props.dispatch(deleteContact(id));
            this.props.dispatch({
              type: 'Navigation/POP'
            })
          }
        }
      ],
      { cancelable: false }
    )

  }
  _keyExtractor = (item, index) => index.toString();

  render() {
    return (
      <View style={styles.container}>
        <FlatList
          data={this.props.contact.data}
          renderItem={this.renderItem}
          keyExtractor={this._keyExtractor}
          extraData={this.state.toggle}
        />
        {/* <Text>{this.props.contact.data[0].name}</Text> */}

        
        <Fab
          active={this.state.active}
          direction="up"
          containerStyle={{}}
          style={{ backgroundColor: '#5067FF' }}
          position="bottomRight"
          onPress={() => this.setState({ active: !this.state.active })}>
          <Icon name="options-vertical" type="SimpleLineIcons" />
          <Button style={{ backgroundColor: '#34A34F' }}
            onPress={this.handleCreate}>
            <Icon name="add-user" type="Entypo" />
          </Button>
          <Button style={{ backgroundColor: '#DD5144' }}
            onPress={() => this.setState({ toggle: !this.state.toggle })}>
            <Icon name="trash" />
          </Button>
        </Fab>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    contact: state.contact
  }
}

export default connect(mapStateToProps)(Contact);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    paddingTop: 5
  },
});