import React from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';
import { Field, reduxForm } from 'redux-form';
import { connect } from 'react-redux';

import TextInput from '../components/redux-form/TextInput';
import { create } from '../../publics/redux/actions/contact';

class ContactCreate extends React.Component {
    constructor(props) {
        super(props);
    }
    
    componentDidMount(){
        this.props.initialize({
            id      :this.props.navigation.getParam('id'),
            name    :this.props.navigation.getParam('name'),
            address :this.props.navigation.getParam('address')
        })
    }

    handleSave = (value) => {
        this.props.dispatch(create(value));
        this.props.dispatch({
            type: 'Navigation/POP'
        })
    }

    render() {
        return (
            <View style={styles.container}>
                <Field
                    name="name"
                    component={TextInput}
                    placeholder="name"
                />

                <Field
                    name="address"
                    component={TextInput}
                    placeholder="address"
                />
                <Text></Text>
                <Button
                    color="#000"
                    title="SAVE"
                    onPress={this.props.handleSubmit(this.handleSave)}
                />
            </View>
        );
    }
}

const amin = "dsadas"
export default reduxForm({
    form: 'profile',
    enableReinitialize : true
})(connect()(ContactCreate));

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    textCounter: {
        fontSize: 100
    }
});
